var appSync = require("./app-sync.js")

// Bước 1: Import module http
var http = require('http');
 
// Bước 2: Khởi tạo server
var server = http.createServer(function(request, response){
    // Biến request: là biến lưu trữ thông tin gửi lên của client
    // Biến response: là biến lưu trữ các thông tin trả về cho client
     
    // Thiết lập Header
    response.writeHead(200, {
        "Context-type" : "text/html"
    });
    var data = appSync.getListDdaIots();
    data = JSON.stringify(data);
    data = JSON.parse(data);
    //data = JSON.stringify(data);
    //data = toString(data);
    console.log(typeof data);
    // Show thông tin
    //response.write(data);
     
    // Kết thúc
    response.end();
});
 
// Bước 3: Lắng nghe cổng 34567 thì thực hiện chương trình
server.listen(34567, function(){
    console.log('Connected Successfull!');
});