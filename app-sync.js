'use strict';
const aws_exports = require('./aws-exports').default;
var date = new Date();
// CONFIG
const AppSync = {
    "url"               : aws_exports.graphqlEndpoint,
    "region"            : aws_exports.region,
    "AUTH_TYPE"         : aws_exports.AUTH_TYPE,
    "apiKey"            : aws_exports.apiKey,
    "accessKeyId"       : aws_exports.accessKeyId,
    "secretAccessKey"   : aws_exports.secretAccessKey
};

// POLYFILLS
global.WebSocket = require('ws');
global.window = global.window || {
    setTimeout: setTimeout,
    clearTimeout: clearTimeout,
    WebSocket: global.WebSocket,
    ArrayBuffer: global.ArrayBuffer,
    addEventListener: function () { },
    navigator: { onLine: true }
};
global.window.localStorage = {
    store: {},
    getItem: function (key) {
        return this.store[key]
    },
    setItem: function (key, value) {
        this.store[key] = value
    },
    removeItem: function (key) {
        delete this.store[key]
    }
};
require('es6-promise').polyfill();
require('isomorphic-fetch');

// Require AppSync module
const AWSAppSyncClient = require('aws-appsync').default;
// If you want to use AWS...
const AWS = require('aws-sdk');
AWS.config.update({
    region : aws_exports.REGION,
    credentials : new AWS.Credentials({
        accessKeyId     : AppSync.accessKeyId,
        secretAccessKey : AppSync.secretAccessKey,
    })
});
const credentials = AWS.config.credentials;
// INIT
// Set up AppSync client
const client = new AWSAppSyncClient({
    url     : AppSync.url,
    region  : AppSync.region,
    auth : {
        type        : AppSync.AUTH_TYPE,
        credentials : credentials
    }
});


// GRAPHQL
const gql = require('graphql-tag');
var driverExist = false;

var getListDdaMqtts = function() {
    var Query = gql(`
    query listDdaMqtts{
        listDdaMqtts{
        items{
            driver_id
            payload
        }
        }
    }
    `);
    // APP CODE
    client.hydrated().then(function (client) {
        // Now run a query
        client.query({ query: Query})
            .then(function log(data) {
                data = JSON.stringify(data);
                data = JSON.parse(data);
                if(data.data.listDdaMqtts) {
                console.log('(Query Data) All Posts ----------->', data.data.listDdaMqtts.items);
                }  
                else {
                    console.log("Error while fetching data");
                }
            })
            .catch(console.error);
    });
}

var getDdaMqttByDriverId = async function(driver_id) {
    var Query = gql(`
    query getDdaMqtt($input: String!){
        getDdaMqtt(driver_id: $input){
            driver_id
        }
    }
    `);
    // APP CODE
    client.hydrated().then(function (client) {
        // Now run a query
        client.query({ query: Query, variables:{input: driver_id} })
            .then(function log(data) {
                
            })
            .catch(console.error);
    });
}

var createMqtt = function(driver_id, payload) {
    // GRAPHQL
    var mutate = gql(`
    mutation createDdaMqtt($input :  CreateDdaMqttInput!) {
        createDdaMqtt(input: $input) {
            driver_id
            payload
        }
      }
    `)

    // APP CODE
    client.hydrated().then(function (client) {
    // Now run a mutation
    const vari = {
        "payload": payload,
        "driver_id": driver_id
    }

    client.mutate({ mutation: mutate, variables:{input: vari} })
    .then(function logData(data) {
        console.log('(Mutate): Inserting Data ----------->', data);
    })
    .catch(console.error);
    });

}

var deleteDdaMqtt = function(driver_id) {
    // GRAPHQL
    var mutate = gql(`
    mutation deleteDdaMqtt($input: DeleteDdaMqttInput!) {
        deleteDdaMqtt(input: $input) {
          driver_id
        }
      }
    `)

    // APP CODE
    client.hydrated().then(function (client) {
    // Now run a mutation
    const vari = {
        "driver_id": driver_id
    }

    client.mutate({ mutation: mutate, variables:{input: vari} })
    .then(function logData(data) {
        console.log('(Mutate): Deleting Data ----------->', data);
    })
    .catch(console.error);
    });

}

var updateDdaMqttByDriverId = function(driver_id, payload) {
    // GRAPHQL
    var mutate = gql(`
    mutation updateDdaMqtt($input: UpdateDdaMqttInput!) {
        updateDdaMqtt(input: $input) {
            driver_id
            payload
        }
    }
    `) 
    // APP CODE
    client.hydrated().then(function (client) {
    // Now run a mutation
    const vari = {
        "driver_id" : driver_id,
        "payload"   : payload
    };

    client.mutate({ mutation: mutate, variables:{input: vari} })
    .then(function logData(data) {
        console.log('(Mutate): updating Data ----------->', data);
    })
    .catch(console.error);
    });
}

var updateDdaMqtt = function(dataObject) {
    //dataObject = JSON.parse(dataObject);
    var driver_id = dataObject.driver_id;
    var payload = '{"latitude" : "' +dataObject.latitude+ '", "longitude" : "' + dataObject.longitude+'","driver_id": "'+driver_id+'", "timestamp": "'+date.getTime()+'","moving_angle" : "'+dataObject.moving_angle+'"}';
    if(!driver_id) {
        console.log('driver_id is not null');
        return;
    }
    // check driver exist
    var Query = gql(`
    query getDdaMqtt($input: String!){
        getDdaMqtt(driver_id: $input){
            driver_id
        }
    }
    `);
    // APP CODE
    client.hydrated().then(function (client) {
        // Now run a query
        client.query({ query: Query, variables:{input: driver_id} })
            .then(function log(data) {
                if (data.data.getDdaMqtt) {
                    // exist = update
                    updateDdaMqttByDriverId(driver_id, payload);
                } else {
                    //create
                    createMqtt(driver_id,payload);
                }
            })
            .catch(console.error);
            return 1;
    });
}
module.exports = {
    updateDdaMqtt : updateDdaMqtt
};